extern crate actix;
extern crate actix_web;
extern crate failure;
extern crate nt;
extern crate serde;
extern crate serde_json;

use actix::prelude::*;
use actix_web::{fs, ws};
use serde::{Deserialize, Serialize};
use std::collections::HashSet;

const NETWORKTABLES_JS: &str = include_str!("js/networktables.js");
const UTILS_JS: &str = include_str!("js/utils.js");
const CAMERA_JS: &str = include_str!("js/camera.js");
const JQUERY_EXT_JS: &str = include_str!("js/jquery_ext.js");

type Result<T, E = failure::Error> = std::result::Result<T, E>;

#[derive(Debug, Clone, Message)]
struct EntryMessage(nt::EntryData);

/// A message representing an entry for a web client.
#[derive(Debug, Clone, Message, Serialize, Deserialize)]
struct ClientEntry {
    k: String,
    v: nt::EntryValue,
}

#[derive(Debug, Serialize, Deserialize)]
struct BorrowedClientEntry<'a> {
    k: &'a str,
    v: nt::EntryValue,
}

/// A message representing a websocket text frame.
#[derive(Debug, Clone, Message)]
struct WSTextMessage(actix_web::Binary);

impl WSTextMessage {
    fn new(text: impl Into<actix_web::Binary>) -> Self {
        WSTextMessage(text.into())
    }
}

/// A message indicating a new web client that has connected.
#[derive(Message)]
struct Connect {
    addr: Recipient<WSTextMessage>,
}
/// A message indicating that a web client has disconnected.
#[derive(Message)]
struct Disconnect {
    addr: Recipient<WSTextMessage>,
}

/// The NetworkTables dispatcher.
struct Conductor {
    socks: HashSet<Recipient<WSTextMessage>>,
    nt: nt::NetworkTables,
}

impl Actor for Conductor {
    type Context = Context<Self>;
}

impl Conductor {
    fn new(nt: nt::NetworkTables, ctx: &mut Context<Self>) -> Self {
        let mut c = Conductor {
            socks: HashSet::new(),
            nt,
        };

        {
            let addr = ctx.address();
            c.nt.add_callback(nt::CallbackType::Add, move |entry| {
                addr.do_send(EntryMessage(entry.clone()))
            });
        }
        {
            let addr = ctx.address();
            c.nt.add_callback(nt::CallbackType::Update, move |entry| {
                addr.do_send(EntryMessage(entry.clone()))
            });
        }

        c
    }
}

impl Handler<Connect> for Conductor {
    type Result = ();

    fn handle(&mut self, msg: Connect, _ctx: &mut Self::Context) {
        let addr = msg.addr;
        self.socks.insert(addr.clone());

        for entry in self.nt.entries().values() {
            let entry = BorrowedClientEntry {
                k: &entry.name,
                v: entry.value.clone(),
            };
            match serde_json::to_vec(&entry) {
                Ok(json) => {
                    let entry_msg = WSTextMessage::new(json);
                    // TODO: handle failure
                    addr.do_send(entry_msg.clone());
                }
                Err(e) => eprintln!("Error in JSON serialization: {}", e),
            }
        }
    }
}
impl Handler<Disconnect> for Conductor {
    type Result = ();

    fn handle(&mut self, msg: Disconnect, _ctx: &mut Self::Context) {
        self.socks.remove(&msg.addr);
    }
}
impl Handler<ClientEntry> for Conductor {
    type Result = ();

    fn handle(&mut self, msg: ClientEntry, _ctx: &mut Self::Context) {
        match self.nt.find_id_by_name(&msg.k) {
            Some(id) => self.nt.get_entry_mut(id).set_value(msg.v),
            None => {
                self.nt.create_entry(nt::EntryData::new(msg.k, 0, msg.v));
            }
        };
    }
}
impl Handler<EntryMessage> for Conductor {
    type Result = ();

    fn handle(&mut self, msg: EntryMessage, _ctx: &mut Self::Context) {
        let entry = ClientEntry {
            k: msg.0.name,
            v: msg.0.value,
        };
        match serde_json::to_vec(&entry) {
            Ok(json) => {
                let msg = WSTextMessage::new(json);
                for sock in &self.socks {
                    // TODO: handle failure
                    sock.do_send(msg.clone());
                }
            }
            Err(e) => eprintln!("Error in JSON serialization: {}", e),
        }
    }
}

struct AppState {
    conductor: Addr<Conductor>,
}

struct Ws;

impl Actor for Ws {
    type Context = ws::WebsocketContext<Self, AppState>;

    fn started(&mut self, ctx: &mut Self::Context) {
        // register self with coordinator
        let addr = ctx.address();
        ctx.state().conductor.do_send(Connect {
            addr: addr.recipient(),
        });
    }

    fn stopping(&mut self, ctx: &mut Self::Context) -> Running {
        // notify coordinator
        let addr = ctx.address();
        ctx.state().conductor.do_send(Disconnect {
            addr: addr.recipient(),
        });
        Running::Stop
    }
}

impl Handler<WSTextMessage> for Ws {
    type Result = ();

    fn handle(&mut self, msg: WSTextMessage, ctx: &mut Self::Context) {
        ctx.text(msg.0)
    }
}

impl StreamHandler<ws::Message, ws::ProtocolError> for Ws {
    fn handle(&mut self, msg: ws::Message, ctx: &mut Self::Context) {
        match msg {
            ws::Message::Text(blob) => {
                let m: ClientEntry = serde_json::from_str(&blob).unwrap();
                ctx.state().conductor.do_send(m);
            }
            _ => (),
        };
    }
}

fn main() -> Result<()> {
    let nt_addr = "127.0.0.1:1735".parse()?;
    let sys = actix::System::new("nt2js");
    let nt = nt::NetworkTables::connect("nt2js-rs", nt_addr).expect("Unable to connect to NT");

    let conductor = Arbiter::start(|ctx| Conductor::new(nt, ctx));

    actix_web::server::new(move || {
        let state = AppState {
            conductor: conductor.clone(),
        };

        let app = actix_web::App::with_state(state)
            // serve our builtin resources
            .resource("/networktables/networktables.js", |r| {
                r.f(|_| NETWORKTABLES_JS)
            })
            .resource("/networktables/utils.js", |r| r.f(|_| UTILS_JS))
            .resource("/networktables/camera.js", |r| r.f(|_| CAMERA_JS))
            .resource("/networktables/jquery_ext.js", |r| r.f(|_| JQUERY_EXT_JS))
            // websocket
            .resource("/networktables/ws", |r| r.f(|req| ws::start(req, Ws)))
            // serve the user's files
            .handler(
                "/",
                fs::StaticFiles::new(".").unwrap().index_file("index.html"),
            );

        app
    })
    .bind("[::]:8080")?
    .start();

    println!("nt2js-rs: Listening on http://localhost:8080");

    sys.run();
    Ok(())
}
